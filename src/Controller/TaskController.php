<?php

namespace App\Controller;

use App\Entity\Task;
use App\Enumeration\TaskState;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

#[Route('/task')]
class TaskController extends AbstractController
{
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    #[Route('/', name: 'task_list')]
    public function list(TaskRepository $taskRepository): Response
    {
        $tasks = $taskRepository->findAll();

        return $this->render('task/list_task.html.twig', [
            'tasks' => $tasks,
            'states' => TaskState::getState()
        ]);
    }

    #[Route('/filter-tasks', name: 'filter_tasks')]
    public function filterTasks(Request $request): Response
    {
        $selectedState = $request->query->get('state');
        $filteredTasks = $this->taskRepository->findByState($selectedState);

        return $this->render('task/list_task.html.twig', [
            'tasks' => $filteredTasks,
            'states' => TaskState::getState()
        ]);
    }

    #[Route('/add/{id<\d+>}', name: 'task_add')]
    public function add(Request $request, ?int $id = null): JsonResponse | RedirectResponse
    {
        if ($id) {
            $task = $this->taskRepository->find($id);
        } else {
            $task = new Task();
        }

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->taskRepository->save($task);

            return $this->redirectToRoute('task_list');
        }

        return new JsonResponse([
            'success' => false,
            'form' => $this->renderView('task/edit.html.twig', [
                'form' => $form->createView(),
                'id' => $id,
            ])
        ]);
    }

    #[Route('/update-state/{id}', name: 'task_update_state', methods: ['POST'])]
    public function updateState(int $id, Request $request): JsonResponse
    {
        $task = $this->taskRepository->find($id);

        if (!$task) {
            return new JsonResponse(['error' => 'Task not found.'], 404);
        }

        $newState = $request->request->get('state');

        if ($newState) {
            $this->taskRepository->updateState($task, $newState);

            return new JsonResponse(['message' => 'Task state updated.', 'state' => $newState]);
        }

        return new JsonResponse(['error' => 'Invalide state.'], 400);
    }

    #[Route('/delete/{id}', name: 'task_delete', methods: ['POST'])]
    public function delete(int $id): JsonResponse
    {
        $task = $this->taskRepository->find($id);

        if (!$task) {
            return new JsonResponse(['error' => 'Task not found.'], 404);
        }

        $this->taskRepository->remove($task);

        return new JsonResponse(['message' => 'Task deleted.']);
    }
}
