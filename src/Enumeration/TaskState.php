<?php

namespace App\Enumeration;


use Garoevans\PhpEnum\Enum;

class TaskState extends Enum {
    public const PROGRESS = 'Progressing';
    public const COMPLETED = 'Completed';
    public const CANCELED = 'Canceled';

    public static function getState() : array
    {
        return [self::PROGRESS, self::COMPLETED, self::CANCELED];
    }
}