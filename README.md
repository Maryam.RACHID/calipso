Spécifications du projet :

●	Une entité "Task" (tâche) avec les propriétés suivantes :
        - ID (identifiant)
        - Titre
        - Description 
        - Date de création
        - État (en cours, terminée, annulée, etc.)

●	Un formulaire permettant d'ajouter une nouvelle tâche. Le formulaire doit comporter des champs pour le titre et la description.

●	Afficher la liste des tâches existantes dans une vue avec les informations suivantes :
        - Titre
        - Description
        - Date de création
        - État
        - Option permettant de marquer une tâche comme terminée ou annulée.

●	Implémenter la fonctionnalité pour marquer une tâche comme terminée ou annulée. La mise à jour de l'état doit être effectuée en utilisant des requêtes asynchrones (AJAX) pour éviter le rechargement de la page.

●	Ajouter une fonctionnalité permettant de filtrer les tâches en fonction de leur état (en cours, terminée, annulée, etc.).Mettez en œuvre une fonctionnalité de pagination pour limiter l'affichage à un nombre défini de tâches par page.

●	Utiliser Bootstrap (ou tout autre framework CSS) pour styliser l'interface utilisateur.
