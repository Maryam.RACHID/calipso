$(document).ready(function() {
    $('.table').DataTable({
        paging: true,       // Activer la pagination
        searching: true,     // Activer la recherche
        columnDefs: [
            { targets: '_all', className: 'text-center' }
        ]
    });
});

$(document).ready(function() {
    $('.update-state').click(function(e) {
        e.preventDefault();

        var taskId = $(this).closest('tr').data('task-id');

        $(this).siblings('.dropdown-menu').find('.dropdown-item').click(function(e) {
            e.preventDefault();

            var newState = $(this).data('state');

            updateTaskState(taskId, newState);
        });
    });

    function updateTaskState(taskId, newState) {
        $.ajax({
            url: '/task/update-state/' + taskId,
            method: 'POST',
            data: { state: newState },
                success: function(response) {
                    console.log('État de la tâche mis à jour :', response);
                    // Mettez à jour l'affichage de l'état de la tâche ici
                    $('#'+taskId).html(response.state);
                    const taskRow = $('[data-task-id="' + taskId + '"]');
                    taskRow.removeClass().addClass(response.state);
                },
            error: function(error) {
                console.error('Erreur lors de la mise à jour de l\'état de la tâche :', error);
            }
        });
    }

    // Initialisation des menus déroulants Bootstrap
    var dropdowns = document.querySelectorAll('.dropdown-toggle');
    dropdowns.forEach(function (dropdown) {
        new bootstrap.Dropdown(dropdown);
    });
});

$(document).ready(function() {
    $('.dropdown-item[data-state="delete-task"]').click(function(e) {
        e.preventDefault();

        var taskId = $(this).data('id');

        deleteTask(taskId);
    });

    function deleteTask(taskId) {
        $.ajax({
            url: '/task/delete/' + taskId,
            method: 'POST',
            success: function(response) {
                console.log('Tâche supprimée :', response);
                $('#' + taskId).closest('tr').remove();
            },
            error: function(error) {
                console.error('Erreur lors de la suppression de la tâche :', error);
            }
        });
    }

    var dropdowns = document.querySelectorAll('.dropdown-toggle');
    dropdowns.forEach(function (dropdown) {
        new bootstrap.Dropdown(dropdown);
    });
});

document.addEventListener("DOMContentLoaded", function () {
    // using vanilla js for the add
    const addButton = document.querySelector(".btn-primary[data-bs-target='#exampleModal']");
    const addModalBody = document.querySelector("#exampleModal .modal-body");

    addButton.addEventListener("click", function () {
        $.ajax({
            url: '/task/add',
            method: 'POST',
            success: function(response) {
                if (!response.success) {
                    addModalBody.innerHTML = response.form; // Display the form
                }
            },
            error: function(error) {
                console.error("Error:", error);
            }
        });
    });

    // using jquery for the edit
    $('.update-task-link').on('click', function() {
        console.log('rear');
        var id = $(this).data('id');
        console.log(id);
        $.ajax({
            url: '/task/add/' + id,
            method: 'POST',
            success: function(response) {
                if (!response.success) {
                    $('#updateModal').find('.modal-body').html(response.form);
                }
            },
            error: function(error) {
                console.error('Error while modifying the task :', error);
            }
        });
    });
});

$(document).ready(function () {
    $('.state-filter').change(function () {
        var selectedState = $(this).val();

        $.ajax({
            url: '/task/filter-tasks/',
            method: 'GET',
            data: { state: selectedState },
            success: function (data) {
                $('#tasks-table tbody').html($(data).find('#tasks-table tbody').html());
            },
            error: function (error) {
                console.error('Error while filtering tasks :', error);
            }
        });
    });
});


